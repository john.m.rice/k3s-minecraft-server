Minecraft Server in Kubernetes
==============
This sets up a minecraft server, running with NFS storage, on a Kubernetes cluster (im using K3s, your mileage may very).

This was taken directly from Solarhess github repo: https://github.com/solarhess/kubernetes-minecraft-server
==============

k8s-mc.sh takend from reference [0].

This demonstrates how to run Minecraft in Kubernetes. It builds on the work of 
the [itzg Minecraft Server Docker Image](https://hub.docker.com/r/itzg/minecraft-server/) 
to create a docker container in kubernetes

This repository demonstrates how to run in a local kubernetes cluster using
local disks. 

How To Install
------

### Prerequisites 
* Have a Kubernetes cluster, be able to SSH to the nodes in your cluster
* Have your `kubeconfig` command line set up to talk to your cluster
* Have cluster admin privileges on this cluster
* Clone this repository to your computer
* Have an NFS server setup

### Step 1: Setup NFS storage

Make sure your NFS storage has the proper folder available. 
In my case my server is located at 
```bash
192.168.1.90
```
And my mount point is 
```bash
/mnt/nfs
```
So I ssh into my storage and run the following to create the needed folder:

```bash
ssh your_nfs_server_ip
mkdir /mnt/nfs/minecraft
chmod 777 /mnt/nfs/minecraft
```

Edit [20-volume-nfs.yaml](20-volume-nfs.yaml#L20). Update Line 22 and 23 to include the nfs server IP. Update line 23 to include the folder path that NFS is sharing.

### Step 2: Configure your Minecraft Server

The [itzg Minecraft Server Docker Image](https://hub.docker.com/r/itzg/minecraft-server/) 
allows you to configure settings in the minecraft
server by setting environment variables. 

You can edit [30-deployment-local.yaml](30-deployment-local.yaml#L36) to add or
change environment variables for your purposes. See lines 35 - like 40 or so, for examples. See the minecraft-server docker image page for documentation on configuring your server.


### Step 3: Apply Kubernetes Configurations

Now, apply the konfigurization file to Kubernetes

```bash
kubectl apply -k . -n minecraft
```
To delete the cluster run the following:
```bash
kubectl delete -k . -n minecraft
```

### Step 4: Connect

We have created a node-port service, which means
that you can connect to any node in your kubernetes cluster on port `30565` (declared in [40-service-nodeport.yaml](40-service-nodeport.yaml#L12)) to attach to this minecraft server. Suppose that your kubernetes node has a public
ip address of 192.168.1.203.  When you configure your
minecraft server, connect to `192.168.1.203:30565`

Invitation to Help
------

If anyone knows Traefik, and would like to contribute, I would love to learn how to set this up with a Traefik proxy/LB and have it pull a DHCP address from my network.

## REFERENCES

[0] k8s-mc.sh: https://github.com/gilesknap/k3s-minecraft/blob/main/k8s-mc.sh
