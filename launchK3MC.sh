#!/bin/bash
# use k8s kustomization file to launch minecraft (or FTB)
kubectl apply -k . -n minecraft
# Make sure to already have the /data mounted to /mnt/nfs locally.
# then, remove the pre-created ops/whitelist/server files
rm -rf /mnt/nfs/minecraft/ops.json /mnt/nfs/minecraft/server.properties /mnt/nfs/minecraft/whitelist.json
# copy over new files, with our info in them
cp ops.json /mnt/nfs/minecraft/
cp server.properties /mnt/nfs/minecraft/
cp whitelist.json /mnt/nfs/minecraft/
# sleep for a few min, so the container has time to build out the world folders
sleep 120
# copy over datapacks and unzip it. this should unzip into / with a archive o mnt/nfs/minecraft/world/datapacks/
unzip datapacks.minecraft-1.19.zip -d /
# 
# To delete just run : kubectl delete -k . -n minecraft
#
